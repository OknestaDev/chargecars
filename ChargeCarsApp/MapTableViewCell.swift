//
//  MapTableViewCell.swift
//  ChargeCarsApp
//
//  Created by Oknesta Developer on 8/16/18.
//  Copyright © 2018 EugeneYatsenko. All rights reserved.
//

import UIKit

class MapTableViewCell: UITableViewCell {

  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var subTitleLabel: UILabel!
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  func configure(title:String,subTitle:Float?) {
    titleLabel.text = title
    if let subTitle = subTitle {
      subTitleLabel.text = String(format: "%.02f км.", subTitle)
    }
  }
}
