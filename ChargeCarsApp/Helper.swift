//
//  Helper.swift
//  ChargeCarsApp
//
//  Created by Oknesta Developer on 8/8/18.
//  Copyright © 2018 EugeneYatsenko. All rights reserved.
//

import Foundation
import UIKit



extension UIViewController {
  
  func showAlert(title: String?, message: String?) {
    self.showAlert(title: title, message: message, cancelTitle: nil, actions: nil)
  }
  
  func showAlert(title: String?, message: String?, cancelTitle: String?, actions: [UIAlertAction]?) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    if let actions = actions {
      for action in actions {
        alert.addAction(action)
      }
    }
    alert.addAction(UIAlertAction(title: cancelTitle ?? "OK", style: .cancel, handler: nil))
    self.present(alert, animated: true, completion: nil)
  }
  
  func showAlert(title: String?, message: String?, actions: [UIAlertAction]?) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    if let actions = actions {
      for action in actions {
        alert.addAction(action)
      }
    }
    self.present(alert, animated: true, completion: nil)
  }
  
}

extension UIView {
  
  @IBInspectable
  var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
      layer.masksToBounds = newValue > 0
    }
  }
  
  @IBInspectable
  var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  @IBInspectable
  var borderColor: UIColor? {
    get {
      let color = UIColor.init(cgColor: layer.borderColor!) 
      return color
    }
    set {
      layer.borderColor = newValue?.cgColor
    }
  }
  
  @IBInspectable
  var shadowRadius: CGFloat {
    get {
      return layer.shadowRadius
    }
    set {
      layer.shadowColor = UIColor.black.cgColor
      layer.shadowOffset = CGSize(width: 0, height: 2)
      layer.shadowOpacity = 0.4
      layer.shadowRadius = shadowRadius
    }
  }
  
}

extension UIView {
  
  func roundTop(radius:CGFloat = 5){
    self.clipsToBounds = true
    self.layer.cornerRadius = radius
    if #available(iOS 11.0, *) {
      self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    } else {
      // Fallback on earlier versions
    }
  }
  
  func roundBottom(radius:CGFloat = 5){
    self.clipsToBounds = true
    self.layer.cornerRadius = radius
    if #available(iOS 11.0, *) {
      self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    } else {
      // Fallback on earlier versions
    }
  }
}
