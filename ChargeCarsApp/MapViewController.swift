//
//  ViewController.swift
//  ChargeCarsApp
//
//  Created by EugeneYatsenko on 4/25/17.
//  Copyright © 2017 EugeneYatsenko. All rights reserved.
//

import UIKit
import AEXML
import GoogleMaps

class POIItem: NSObject, GMUClusterItem {
  var position: CLLocationCoordinate2D
  var name: String!
  
  init(position: CLLocationCoordinate2D, name: String) {
    self.position = position
    self.name = name
  }
}

class ColorStyle {
  enum ColorStyleList: Int {
    case white = 0
    case dark = 1
  }
  
  func color(colorStyleList:ColorStyleList,view:UIView) {
    switch colorStyleList {
    case .dark:
      UIApplication.shared.statusBarStyle = .lightContent
      if let label = view as? UILabel {
        label.textColor = .white
        return
      }
      if let navBar = view as? UINavigationBar {
        navBar.barTintColor = #colorLiteral(red: 0.1607843137, green: 0.1647058824, blue: 0.1843137255, alpha: 1)
        return
      }
      view.backgroundColor = #colorLiteral(red: 0.1607843137, green: 0.1647058824, blue: 0.1843137255, alpha: 1)
    default:
      UIApplication.shared.statusBarStyle = .default
      if let label = view as? UILabel {
        label.textColor = .black
        return
      }
      if let navBar = view as? UINavigationBar {
        navBar.barTintColor = .white
        return
      }
      view.backgroundColor = .white
    }
  }
  
}



class MapViewController: UIViewController {
  
  private let viewModel = MapViewModel()
  private let colorStyle = ColorStyle()
  private var topInset:CGFloat = 0.0
  private var clusterManager: GMUClusterManager!
  private var locationManager = CLLocationManager()
  private let segment = UISegmentedControl(items: ["Карта","Гибрид"])
  private var gmap:GMSMapView! {
    didSet  {
      gmap.delegate = self
    }
  }
  
  private var locationButton:UIButton = {
    let button = UIButton()
    button.backgroundColor = .white
    button.layer.cornerRadius = 30
    button.setImage(#imageLiteral(resourceName: "iam"), for: .normal)
    button.addTarget(self, action: #selector(myLocationTarget), for: .touchDown)
    return button
  }()
  
  private var mylocation: CLLocation? {
    didSet {
      if let location = mylocation {
        viewModel.pointNear = []
        tableView.reloadData()
        viewModel.distance(location: location) { [weak self] in
          guard let `self` = self else {return}
          DispatchQueue.main.async {
            self.tableView.reloadData()
          }
        }
      }
    }
  }

  @IBOutlet weak var topTableViewView: UIView! {
    didSet {
      topTableViewView.roundTop(radius: 5)
    }
  }
  @IBOutlet weak var arrowImage: UIImageView!
  @IBOutlet weak var tableView: UITableView! {
    didSet {
      tableView.bounces = false
      tableView.tableFooterView  = UIView()
    }
  }
  

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    locationButton.frame = CGRect(x: view.frame.size.width-75, y: view.frame.size.height*0.62, width: 60, height: 60)
    navigationController?.navigationBar.isHidden = true
    configureSegControl()
    mapAddedToBackTableView()
  }
  
  
  private func prepare() {
    createClusterManager()
    getPoints()
    getDataFromServer()
    locationManager.delegate = self
  }
  
  private func createClusterManager() {
    // Set up the cluster manager with the supplied icon generator and
    // renderer.
    guard let gmap = gmap else {return}
    let iconGenerator = GMUDefaultClusterIconGenerator()
    let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
    let renderer = GMUDefaultClusterRenderer(mapView: gmap,
                                             clusterIconGenerator: iconGenerator)
    clusterManager = GMUClusterManager(map: gmap, algorithm: algorithm,
                                       renderer: renderer)

    
    // Generate and add random items to the cluster manager.
    generateClusterItems()
    
    // Call cluster() after items have been added to perform the clustering
    // and rendering on map.
    clusterManager.cluster()
    clusterManager.setDelegate(self, mapDelegate: self)
    renderer.delegate = self
  }
  
  private func generateClusterItems() {
    for enumCase in self.viewModel.pointsModel {
      switch enumCase {
      case .point(let point):
        let item = POIItem(position: CLLocationCoordinate2DMake(point.lat, point.lon), name: point.titelPoint)
        clusterManager.add(item)
      default:
        break
      }
    }
  }
  
  private func getPoints() {
    viewModel.getPoints { [weak self] in
      guard let `self` = self else {return}
      DispatchQueue.main.async {
        self.generateClusterItems()
      }
      DispatchQueue.main.async {
        self.locationManager.startUpdatingLocation()
      }
    }
  }
  
  private func createMarkers() {
    guard let gmap = self.gmap else {return}
    for enumCase in self.viewModel.pointsModel {
      switch enumCase {
      case .point(let point):
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: point.lat, longitude: point.lon)
        marker.title = point.titelPoint
        marker.icon = UIImage(named: markerImage)
        DispatchQueue.main.async {
          marker.map = gmap
        }
      default:
        break
      }
    }
  }
  
  private func mapAddedToBackTableView() {
   if self.gmap == nil {
      gmap = GMSMapView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.frame.size.height*0.95))
      guard let gmap = gmap else {return}
      gmap.delegate = self
      let containerView = UIView(frame: tableView.frame)
      let camera = GMSCameraPosition.camera(withLatitude: Double(50.4558046) , longitude: Double(30.5126752 ), zoom: 4.8)
      gmap.animate(to: camera)
      gmap.isMyLocationEnabled = true
      gmap.addSubview(locationButton)
      containerView.addSubview(gmap)
      configureTableView(containerView:containerView)
    }
    prepare()
  }
  
  private func configureSegControl() {
    segment.sizeToFit()
    segment.tintColor = #colorLiteral(red: 0.168627451, green: 0.7411764706, blue: 0.8862745098, alpha: 1)
    segment.selectedSegmentIndex = 0;
    segment.addTarget(self, action: #selector(chartTypeChanged(_:)), for: .valueChanged)
    self.navigationItem.titleView = segment
  }
  
  @objc func chartTypeChanged(_ segControl: UISegmentedControl) {
    if segment.selectedSegmentIndex == 0 {
      gmap?.mapType = .normal
    } else {
      gmap?.mapType = .hybrid
    }
    colorChange()
  }
  
  private func colorNavBar(color:UIBarStyle) {
    navigationController?.navigationBar.barStyle = color
    navigationController?.navigationBar.tintColor = UIColor.white
  }

  
  private func colorChange() {
    guard
      let rawValue = ColorStyle.ColorStyleList(rawValue: segment.selectedSegmentIndex)
    else {return}
      colorStyle.color(colorStyleList: rawValue, view: tableView)
      colorStyle.color(colorStyleList: rawValue, view: topTableViewView)
      colorStyle.color(colorStyleList: rawValue, view: view)
      colorStyle.color(colorStyleList: rawValue, view: locationButton)
      if let navBar = navigationController?.navigationBar {
       colorStyle.color(colorStyleList: rawValue, view: navBar)
      }
      tableView.reloadData()
  }
  
  private func configureTableView(containerView:UIView) {
    tableView.backgroundView = containerView
    tableView.contentInset = UIEdgeInsetsMake(view.frame.size.height*0.70, 0, 0, 0)
    topInset = view.frame.size.height*0.73
  }
  
  @objc private func myLocationTarget() {
    UIView.animate(withDuration: 0.3) {
      self.tableView.contentOffset.y = -self.topInset
    }
    if let myLocation = gmap?.myLocation {
      toMyLocation(myLocation: myLocation)
    } else {
      let alert = UIAlertController(title: "Геолокация", message: "Для определения местоположения разрешите использовать вашу геолокацию?", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Хорошо", style: .cancel, handler: nil))
      self.present(alert, animated: true, completion: nil)
    }
  }
  
  private func toMyLocation(myLocation: CLLocation) {
    let camera = GMSCameraPosition.camera(withLatitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude, zoom: 12.0)
    self.gmap?.animate(to: camera)
    viewModel.pointNear = []
    tableView.reloadData()
    viewModel.distance(location: myLocation) { [weak self] in
      guard let `self` = self else {return}
      DispatchQueue.main.async {
        self.tableView.reloadData()
      }
    }
  }
  
  func getDataFromServer() {
    getDataFromJson { result in
      //print(result)
    }
  }
}

extension MapViewController: UITableViewDataSource {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.pointNear.count
  }
  
  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch viewModel.pointNear[indexPath.row] {
    case .header(let title):
      let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell") as! HeaderTableViewCell
      if let rawValue = ColorStyle.ColorStyleList(rawValue: segment.selectedSegmentIndex) {
        colorStyle.color(colorStyleList: rawValue, view: cell)
        colorStyle.color(colorStyleList: rawValue, view: cell.titleLabel)
      }
      cell.configure(title: title)
      return cell
    case .point(let point):
      let cell = tableView.dequeueReusableCell(withIdentifier: "MapTableViewCell") as! MapTableViewCell
      if let rawValue = ColorStyle.ColorStyleList(rawValue: segment.selectedSegmentIndex) {
        colorStyle.color(colorStyleList: rawValue, view: cell)
        colorStyle.color(colorStyleList: rawValue, view: cell.titleLabel)
      }
      cell.configure(title: point.titelPoint, subTitle:  point.distance)
      return cell
    }
  }
}

extension MapViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    switch viewModel.pointNear[indexPath.row] {
    case .header:
      return 40
    default:
      return 60
    }
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if self.tableView.contentOffset.y < -(view.frame.size.height/2) {
      arrowImage.image = #imageLiteral(resourceName: "default")
    } else {
      arrowImage.image = #imageLiteral(resourceName: "down")
    }
    self.navigationController?.setNavigationBarHidden(self.tableView.contentOffset.y < -20, animated: true)
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch viewModel.pointNear[indexPath.row] {
    case .point(let point):
      let coordinate = CLLocationCoordinate2D(latitude: point.lat, longitude: point.lon)
      goToLocation(coordinate: coordinate)
    default:
      break
    }
   
  }
  
  private func goToLocation(coordinate: CLLocationCoordinate2D) {
    let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 16.0)
    self.gmap?.animate(to: camera)
    UIView.animate(withDuration: 0.3) {
      self.tableView.contentOffset.y = -self.topInset
    }
  }

}
 
extension MapViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let location = locations.last
    guard
      let latitude = location?.coordinate.latitude,
      let longitude = location?.coordinate.longitude
      else {return}
    let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 12.0)
    self.gmap?.animate(to: camera)
    self.locationManager.stopUpdatingLocation()
  
    if mylocation == nil {
       mylocation = location
    }
  }
}

extension MapViewController:  GMUClusterManagerDelegate, GMSMapViewDelegate,GMUClusterRendererDelegate {
  func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {

    let alertRoute = UIAlertController(title: "Маршрут", message: "Построить маршрут?", preferredStyle: .alert)
    alertRoute.addAction(UIAlertAction(title: "Построить", style: .default , handler: { [weak self] (action) in
      guard let `self` = self else {return}
      if (UIApplication.shared.canOpenURL(URL(string:comgooglemaps)!)) {
        guard
          let mylatitude = mapView.myLocation?.coordinate.latitude,
          let mylongitude = mapView.myLocation?.coordinate.longitude
        else {
          DispatchQueue.main.async {
            self.showAlert(title: "Включите геолокацию", message: "Ваше местоположение не найдено");
          }
          return}
        UIApplication.shared.openURL(URL(string:"comgooglemaps://?saddr=\(mylatitude),\(mylongitude)&daddr=\(marker.position.latitude),\(marker.position.longitude)&directionsmode=driving")!)
      } else {
        guard
          let mylatitude = mapView.myLocation?.coordinate.latitude,
          let mylongitude = mapView.myLocation?.coordinate.longitude
          else {
            DispatchQueue.main.async {
              self.showAlert(title: "Включите геолокацию", message: "Ваше местоположение не найдено");
            }
          return}
        print("Can't use comgooglemaps://");
        UIApplication.shared.openURL(URL(string: "http://maps.apple.com/?saddr=\(mylatitude),\(mylongitude)&daddr=\(marker.position.latitude),\(marker.position.longitude)"
          )!)
      }

    }))
    alertRoute.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
    self.present(alertRoute, animated: true, completion: nil)

  }

  func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
    if let poiItem = marker.userData as? POIItem {
      let infoWindow = Bundle.main.loadNibNamed(infoWindowNib, owner: self, options: nil)?.first as! InfoView
      infoWindow.layer.cornerRadius = 3.0
      infoWindow.imageViewMarker.image = UIImage(named: infoWindowImage)
      infoWindow.textLabel.text = poiItem.name
      return infoWindow
    }
    return nil
  }
  
  func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
    let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                             zoom: gmap.camera.zoom + 1)
    let update = GMSCameraUpdate.setCamera(newCamera)
    gmap.moveCamera(update)
    return false
  }
  
  // MARK: - GMUMapViewDelegate
  
  func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    if let poiItem = marker.userData as? POIItem {
      NSLog("Did tap marker for cluster item \(poiItem.name)")
    } else {
      NSLog("Did tap a normal marker")
    }
    return false
  }
  
  func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
    if let userData = marker.userData {
      //print(userData)
    }
    guard let _ = marker.userData as? POIItem else { return }
    
    if let image = UIImage(named: markerImage) {
      marker.iconView = UIImageView(image: image, highlightedImage: nil)
    }
  }
  
}


