//
//  MapViewModel.swift
//  ChargeCarsApp
//
//  Created by Oknesta Developer on 8/14/18.
//  Copyright © 2018 EugeneYatsenko. All rights reserved.
//

import Foundation
import CoreLocation

class MapViewModel {
  
  enum TypeCell {
    case header(title:String)
    case point(point:PModel)
  }
  
  var pointsModel = [TypeCell]()
  var pointNear = [TypeCell]()
  
  func getPoints(completed: @escaping () -> Void) {
    getDataFromXML { [weak self] result in
      guard let `self` = self else {return}
      var test = result
      test.sort { $0. }
      self.pointsModel.append(.header(title:"Все заправки"))
      result.forEach({  (point) in
        self.pointsModel.append(.point(point: point))
      })
      self.pointNear = self.pointsModel
      completed()
    }
  }
  
  func distance(location:CLLocation,completed: @escaping () -> Void) {
    pointNear.append(.header(title:"Заправки рядом с вами"))
    for caseEnum in pointsModel {
      switch caseEnum {
      case .point(let point):
        let distance = location.distance(from: point.location)
         if (Float(distance)/1000) < 15 {
              point.distance = Float(distance)/1000.0
              pointNear.append(caseEnum)
         }
      default:
        break
      }
    }
    
    
    if pointNear.count == 1 {
      pointNear = []
      pointsModel.append(.header(title:"Все заправки"))
      pointNear = pointsModel
    }
    completed()
  }
}
