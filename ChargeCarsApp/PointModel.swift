//
//  PointModel.swift
//  ChargeCarsApp
//
//  Created by EugeneYatsenko on 4/25/17.
//  Copyright © 2017 EugeneYatsenko. All rights reserved.
//

import Foundation
import GoogleMaps

class PModel {
  var titelPoint:String
  var lon:Double
  var lat:Double
  var location:CLLocation
  var distance:Float?
  
  
  init(_ titelPoint: String, _ lon: Double, _ lat: Double,location:CLLocation) {
    self.titelPoint = titelPoint
    self.lon = lon
    self.lat = lat
    self.location = location
  }
  

}
