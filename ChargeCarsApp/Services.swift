//
//  Services.swift
//  ChargeCarsApp
//
//  Created by EugeneYatsenko on 4/25/17.
//  Copyright © 2017 EugeneYatsenko. All rights reserved.
//

import Foundation
import Alamofire
import AEXML
import SwiftyJSON
import CoreLocation

 func getDataFromXML(_ complete: @escaping ([PModel])->()) {
  guard
    let xmlPath = Bundle.main.path(forResource: "АвтоЭнтерпрайз СЗС.kml", ofType: "xml"),
    let data = try? Data(contentsOf: URL(fileURLWithPath: xmlPath))
    else {
      print("resource not found!")
      return
  }
  guard
    let xmlPathToka = Bundle.main.path(forResource: "АвтоЭнтерпрайз СЗС.kml", ofType: "xml"),
    let dataToka = try? Data(contentsOf: URL(fileURLWithPath: xmlPathToka))
    else {
      print("resource not found!")
      return
  }
  var options = AEXMLOptions()
  options.parserSettings.shouldProcessNamespaces = false
  options.parserSettings.shouldReportNamespacePrefixes = false
  options.parserSettings.shouldResolveExternalEntities = false
  do {
    let xmlDocAZS = try AEXMLDocument(xml: data, options: options)
    let xmlDocToka = try AEXMLDocument(xml: dataToka, options: options)
    let pmodels = factoryPointModels(xmlDocs: [xmlDocAZS,xmlDocToka])
    complete(pmodels)
  } catch {
    print("\(error)")
  }
}


private func factoryPointModels(xmlDocs:[AEXMLDocument]) -> [PModel]  {
  var arrayPoint = [PModel]()
  xmlDocs.forEach { (xmlDoc) in
    for object in xmlDoc.root["Document"]["Folder"]["Placemark"].all ?? [] {
      let pointModel = PModel("", 0.0, 0.0, location: CLLocation(latitude: 0.0, longitude: 0.0))
      if object.count != 0 {
        for placemark in object.children {
          if placemark.name == "name" {
            //print(placemark.value!)
            pointModel.titelPoint = placemark.value!
          } else if placemark.name == "Point"{
            for point in placemark.children {
              if point.name == "coordinates" {
                //print(point.value!)
                if let pointValue = point.value {
                  let componentArray = pointValue.components(separatedBy: ",")
                  if componentArray.count >= 2 {
                    if let lon = Double(componentArray[0]), let lat = Double(componentArray[1]) {
                      pointModel.lon = lon
                      pointModel.lat = lat
                      pointModel.location = CLLocation(latitude: lat, longitude: lon)
                    }
                  }
                }
              }
            }
          }
        }
      }
      if !pointModel.titelPoint.isEmpty,
          pointModel.lat != 0.0,
          pointModel.lon != 0.0 {
        arrayPoint.append(pointModel)
      }
    }
  }
  return arrayPoint
}


 func getDataFromJson(_ complete: @escaping ([PModel])->()) {
  Alamofire.request(requestData, method: .get, parameters: nil, encoding: URLEncoding(), headers: nil) .responseJSON { (respose) in
    switch respose.result{
    case .success(let value):
      let JsonData = JSON(value)
      print(JsonData)
    case .failure(let error):
      print(error)
    }
  }
}
